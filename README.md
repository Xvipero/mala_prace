# Projekt malá práce

## K čemu slouží

Tento projekt slouží k automatizaci tvorby a nasazení statických webových stránek pomocí generátoru statických stránek MKdocs. Projekt zahrnuje nastavení CI/CD pipeline v Gitlabu, která automaticky kontroluje kvalitu Markdown souborů, generuje HTML stránky a nasazuje je na web.

## Jak se používá

1. Nakolonování repozitáře.
2. Instalování potřebných závislostí.
3. Úprava nebo přidání Markdown souborů v adresáři "docs/".
4. Spuštení skriptu pro generování stránek.
5. Provedení testu kvality Markdown souborů.